#include <iostream>
#include <limits>
#include <iomanip>

using namespace std;

const int size = 5;
const int xed = std::numeric_limits<int>::min();

void print_matrix(int matrix[size][size])
{
	for(int i=0; i<size; i++)
	{
		cout<<'|';
		for(int j=0; j<size; j++)
		{
			if(matrix[i][j] != xed)
			{
				cout<<setw(2)<<matrix[i][j];
			}
			else
			{
				cout<<setw(2)<<'X';
			}
			cout<<'|';
		}
		cout<<endl;
	}
}

void xing(int matrix[size][size])
{
	for(int i=0; i<size; i++)
	{
		for(int j=0; j<size; j++)
		{
			if(i == j ||
				 size-1-i == j)
			{
				matrix[i][j] = xed;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	int matrix[size][size];

	int val = 0;
	for(int i=0; i<size; i++)
	{
		for(int j=0; j<size; j++)
		{
			matrix[i][j] = val++;		
		}
	}

	print_matrix(matrix);

	cout<<"After Xing"<<endl;
	xing(matrix);

	print_matrix(matrix);
}
