#include <iostream>
#include <string>

using namespace std;

typedef unsigned int uint;

enum HealthClass
{
	A,
	B,
	C,
	D
};

struct Cat
{
	string name;
	int age;
	HealthClass health_class;
};

void initializeCat(Cat &cat, string name, int age, HealthClass hc)
{
	cat.name = name;
	cat.age = age;
	cat.health_class = hc;
}

void copyCat(Cat &dst, Cat &src)
{
	dst.name = src.name;
	dst.age = src.age;
	dst.health_class = src.health_class;
}

void printCat(Cat &cat)
{
	cout<<"name: "<<cat.name<<endl;
	cout<<"age: "<<cat.age<<endl;
	cout<<"health class: "<<cat.health_class<<endl;
}

bool equalCats(Cat &cat1, Cat &cat2)
{
	return cat1.name == cat2.name &&
				 cat1.age == cat2.age &&
				 cat1.health_class == cat2.health_class;
}

struct ListElement
{
	Cat cat;
	ListElement *next;
	ListElement *prev;
};

void initializeListElement(ListElement &element, Cat &cat)
{
	element.cat = cat;
	element.next = NULL;
	element.prev = NULL;
}

struct List
{
	ListElement *first;
	ListElement *last;
	uint cnt;
};

void pushFirst(List &list, Cat &cat)
{
	cout<<"push first"<<endl;
	ListElement *element = new ListElement();
	initializeListElement(*element, cat);

	list.first = element;
	list.last = element;
	list.cnt++;
}

void push(List &list, Cat &cat)
{
	cout<<"push"<<endl;
	if(list.cnt == 0)
	{
		pushFirst(list, cat);
		return;
	}

	//ListElement* curr = list.first;

	//while(curr->next != NULL)
	//{
		//curr = curr->next;
	//}
	//
	ListElement *next_element = new ListElement();
	initializeListElement(*next_element, cat);

	list.last->next = next_element;
	next_element->prev = list.last;

	list.last = list.last->next;
	list.cnt++;
};

Cat pop(List &list)
{
	if(list.cnt == 0)
	{
		return Cat();
	}
	if(list.cnt == 1)
	{
		ListElement *last = list.last;
		Cat cat = last->cat;
		list.last = NULL;
		list.first = NULL;
		list.cnt--;
		delete last;

		return cat;
	}

	ListElement *last = list.last;
	ListElement *prev = last->prev;
	prev->next = NULL;

	list.last = list.last->prev;
	list.cnt--;

	Cat cat = last->cat;

	delete last;
	return cat;	
};

bool isEmpty(List &list)
{
	return (list.cnt == 0);
};

void initializeList(List &list)
{
	list.first = NULL;
	list.last = NULL;
	list.cnt = 0;
}

void printList(List &list)
{
	ListElement *next = list.first;
	
	cout<<"--------------"<<endl;
	cout<<"List of cats("<<list.cnt<<"):"<<endl;

	ListElement* curr = list.first;

	if(curr == NULL)
	{
		return;
	}

	while(curr->next != NULL)
	{
		curr = curr->next;
	}
	while(next != NULL)
	{
		cout<<"--------------"<<endl;
		printCat(next->cat);
		next = next->next;
	}
}

void destroyList(List &list)
{
	if(list.cnt == 0)
	{
		return;
	}

	ListElement* curr = list.first;
	
	while(curr != NULL)
	{
		ListElement *next = curr->next;
		delete curr;
		curr = next;
	}
}

int main(int argc, char *argv[])
{
	List list;
	initializeList(list);

	cout<<"Is empty : "<<(isEmpty(list) == true ? "Yes" : "No")<<endl;

	Cat cat;
	initializeCat(cat, "Stefan", 5, A);

	push(list, cat);

	cout<<"Is empty : "<<(isEmpty(list) == true ? "Yes" : "No")<<endl;

	Cat cat2;
	initializeCat(cat2, "Adam", 7, C);
	push(list, cat2);

	Cat cat3;
	initializeCat(cat3, "Edeltrauda", 10, D);
	push(list, cat3);
	
	printList(list);

	Cat last_cat = pop(list);
	cout<<"Equal: "<<equalCats(last_cat, cat3)<<endl;

	last_cat = pop(list);
	last_cat = pop(list);
	printList(list);

	push(list, cat3);

	destroyList(list);
	return 0;
}
