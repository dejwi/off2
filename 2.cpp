#include <iostream>

using namespace std;

const char* ostznak(const char *str, char c)
{
	const char *ret = 0;
	const char *curr = str;
	while (*curr != 0)
	{
		if(*curr == c)
		{
			ret = curr;
		}
		curr++;
	}
	return ret;
}

int main(int argc, char *argv[])
{
	cout<<ostznak("Hello World", 'o')<<endl;
	return 0;
}
